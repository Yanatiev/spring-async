### Implementation manged by config DI

##### declare Service interface

![interface](doc/flexible/00.PNG)


##### Some configuration variable which order to use specific implementation

![interface](doc/flexible/01.PNG)


#### Two Spring service implementation conditionally instantiated driven by config

![image](doc/flexible/02.PNG)

![image](doc/flexible/03.PNG)

#### And happy service consumer which doesn't know which service implementation it use

![image](doc/flexible/04.PNG)