package com.yanat.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "super-puper-executor-config", ignoreUnknownFields = true)
public class ExecutorProperties {
    Integer corePoolSize = 10;
    Integer maxPoolSize = 10;
    Integer queueCapacity = 100;
    String threadName = "XXX";
}
