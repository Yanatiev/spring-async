package com.yanat.config;

import com.yanat.config.properties.ExecutorProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * https://www.baeldung.com/spring-async
 * https://www.baeldung.com/java-threadpooltaskexecutor-core-vs-max-poolsize
 */
@Configuration
@EnableAsync
public class AsyncConfig {
    private final ExecutorProperties props;

    public AsyncConfig(ExecutorProperties props) {
        this.props = props;
    }

    /**
     * @return named executor with specific configuration
     */
    @Bean(name = "superPuperTaskExecutor")
    public Executor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(props.getCorePoolSize());
        executor.setMaxPoolSize(props.getMaxPoolSize());
        executor.setQueueCapacity(props.getQueueCapacity());
        executor.setThreadNamePrefix(props.getThreadName());
        executor.initialize();

        return executor;
    }
}
