package com.yanat.model;

import lombok.Data;

@Data
public class Message {
    String message;
}
