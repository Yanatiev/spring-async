package com.yanat.web.rest;

import com.yanat.model.Message;
import com.yanat.service.SlowpokeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api")
public class InMessageController {

    private final SlowpokeService service;

    public InMessageController(SlowpokeService service) {
        this.service = service;
    }

    @PostMapping
    public void processInputMessage(@RequestBody Message message) {
        log.debug(message.toString());
        service.doSomething(message);
    }
}
