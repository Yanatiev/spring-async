package com.yanat.service;

import com.yanat.model.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SlowpokeService {

    @Async("superPuperTaskExecutor")
    public void doSomething(Message message) {
        log.debug("process: {} START", message);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            log.error("{}", e.getMessage());
        }
        log.debug("process: {} FINISH", message);
    }
}
