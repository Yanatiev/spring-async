package com.yanat.service.flex;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@ConditionalOnProperty(name = "flexible-svc.implementation", havingValue = "IMPL_A")
public class FlexibleSvcImplA implements FlexibleSvc {
    public FlexibleSvcImplA() {
        log.info("-----------> IMPL_A created");
    }

    @Override
    public String execute() {
        return "=============> IMPL_A";
    }
}
