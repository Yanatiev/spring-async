package com.yanat.service.flex;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class FlexibleSvcConsumer {
    private final FlexibleSvc flexibleSvc;

    public FlexibleSvcConsumer(FlexibleSvc flexibleSvc) {
        this.flexibleSvc = flexibleSvc;
        log.info("Service Consumer: {}", flexibleSvc.execute());
    }
}
