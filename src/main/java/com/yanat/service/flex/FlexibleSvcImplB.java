package com.yanat.service.flex;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@ConditionalOnProperty(name = "flexible-svc.implementation", havingValue = "IMPL_B")
public class FlexibleSvcImplB implements FlexibleSvc {
    public FlexibleSvcImplB() {
        log.info("-----------> IMPL_B created");
    }

    @Override
    public String execute() {
        return "=============> IMPL_B";
    }
}
