package com.yanat;

import com.yanat.config.properties.ExecutorProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;

@Slf4j
@SpringBootApplication
@EnableConfigurationProperties({ExecutorProperties.class})
public class SpringAsyncApp {

	public static void main(String[] args) {

		ApplicationContext applicationContext = SpringApplication.run(SpringAsyncApp.class, args);

		ExecutorProperties props = applicationContext.getBean(ExecutorProperties.class);

		log.info("==================================================");
		log.info(props.toString());
		log.info("==================================================");
	}
}
