## SPRING thread pool executor usage
### executor pool configuration:
![executor pool configuration](doc/executorConfig.PNG)

### slow service with method marked to execute asynchronously using specific executor pool:
![slow service](doc/slowService.PNG)

### just inject slow service in the other service or controller and invoke service method:
![invoke service method in thread asynchronously](doc/slowServiceInjectionAndInvokeAsync.PNG)

<br/><br/><br/><br/><br/>

## little bit of sugar
### move configuration of the thread pool executor to the config
![config](doc/config.PNG)

### and load it using SPRING configuration properties auto load
![load config](doc/loadConfigInSpring.PNG)



##access swagger

http://localhost:9100/swagger-ui.html

##useful links
https://www.baeldung.com/spring-async
https://www.baeldung.com/java-threadpooltaskexecutor-core-vs-max-poolsize


## DI managed by configuration example

[link to configurable DI manual](FLEX_README.md)